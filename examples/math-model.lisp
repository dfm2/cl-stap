;; to start math server: nc -l -p 6666 -c 'python math.py'

;; to run this example, start server as above and then eval
;; (stap:run-task :class 'math-model)

(ql:quickload :cl-stap)

(import '(optima:match alexandria:if-let))

#-ACT-R
(load "/home/dfm/w/actr7/load-act-r")

(defclass math-model (stap:task)
  ((numbers :accessor math-model-numbers :initform nil)
   (result :accessor math-model-result :initform nil)))

(defmethod stap:update-display :after ((task math-model) value)
  (optima:match (first value)
    ((list key (cons :i _) (cons label _))
     (send-sum task)
     (stap:send task key `((,label . 3)))))) ; 3 is magic number for pressing a button

(defun send-sum (task)
   (loop with sum = nil
         with key = nil
         for thing in (stap:task-display task)
         do (match (first thing)
              ((list "Add these numbers" (cons _ x) (cons _ y))
               (setf (math-model-numbers task) (list x y))
               (setf sum (addition-model-query task x y))
               (setf (math-model-result task) sum))
              ((list label (list :ix))
               (setf key label)))
         finally (progn
                   (assert sum)
                   (assert key)
                   (stap:send task key (prin1-to-string sum)))))

(defmethod stap:deliver-feedback ((task math-model) success)
  (addition-model-feedback task success))

(defparameter *actr-timeout* 10)

(defun addition-model-query (task x y)
  (goal-focus-fct (first (define-chunks-fct
                           `((isa goal state get term-1 ,x term-2 ,y result nil)))))
  (run *actr-timeout*)
  (setf (math-model-result task)
        (if-let ((retrieved (no-output (first (buffer-chunk retrieval)))))
          (chunk-slot-value-fct retrieved 'sum)
          (+ x y (random 2)))))

(defun addition-model-feedback (task success)
  (with-accessors ((numbers math-model-numbers) (result math-model-result)) task
    (format t (if success "Yes, ~D + ~D = ~D~%" "No, ~D + ~D does not equal ~D~%")
            (first numbers) (second numbers) result)
    (when success
      (mod-focus-fct `(state put term-1 ,(first numbers) term-2 ,(second numbers) result ,result))
      (run *actr-timeout*))))

(clear-all)

(define-model addition-model

  (sgp :v nil)

  (chunk-type addition-fact addend-1 addend-2 sum)

  (chunk-type goal state term-1 term-2 result)

  (p get
     =goal>
       isa goal
       state get
       term-1 =num1
       term-2 =num2
   ==>
     =goal>
       state nil
     +retrieval>
       isa addition-fact
       addend-1 =num1
       addend-2 =num2)

  (p put
     =goal>
       isa goal
       state put
       term-1 =num1
       term-2 =num2
       result =num3
   ==>
     +goal>
       isa addition-fact
       addend-1 =num1
       addend-2 =num2
       sum =num3)

   (p finish-put
      =goal>
        isa addition-fact
        addend-1 =num1
        addend-2 =num2
        sum =num3
    ==>
    -goal>)

)
