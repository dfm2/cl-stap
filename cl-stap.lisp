;;; Copyright (c) 2016 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defpackage :cl-stap
  (:nicknames :stap)
  (:use :common-lisp :alexandria :iterate :optima)
  (:export :deliver-feedback :display-wait :run :run-task :send :task
           :task-display :task-feedback :task-goal :task-good-feedback :task-goal
           :task-host task-port task-version :update :update-display))

(in-package :cl-stap)

(defparameter *verbose* nil)

(defun verbose (format &rest args)
  (when *verbose*
    (format t "~&;; [cl-stap] ~?~%" format args)))

(define-constant +whitespace+ '(#\space #\newline #\tab #\return #\page) :test #'equal)

;; Unfortunately st-json has a bug where it is unable to recognize the end of a JSON
;; object without reading a whole lot more first, making it rather difficult to use in
;; a streaming context. So we limit ourselves to JSON objects followed by a new line.
;; TODO Investigate further alternatives to st-json (cl-json is worse; yason has its own
;;      problems, but may be able to be made to work in this context).
(defun read-stap (&optional (stream *standard-input*))
  (iter (with lines := (make-array 100 :element-type 'character
                                   :adjustable t :fill-pointer 0))
        (for (values line missing-newline) := (read-line stream nil))
        (while line)
        (verbose "read ~S" line)
        (format lines "~A~:[~%~;~]" line missing-newline)
        (handler-case
            (return (values (unpack-stap (st-json:read-json-from-string lines)) t))
          (st-json:json-eof-error ()))
        (finally (if (zerop (length (string-trim +whitespace+ lines)))
                     (return (values nil nil))
                     (error "End of file encountered while parsing multi-line JSON ~A"
                            lines)))))

(defun unpack-stap (json)
  (etypecase json
    (st-json:jso (mapcar #'(lambda (pair)
                             (cons (stap-key (car pair)) (unpack-stap (cdr pair))))
                         (slot-value json 'st-json::alist)))
    (list (iter (for elem :in json)
                (collect (cond ((consp elem)
                                (unless (and (consp (cdr elem)) (null (cddr elem)))
                                  (error "Illegal array structure in STAP ~S."
                                         (coerce elem 'vector)))
                                (cons (stap-key (first elem)) (unpack-stap (second elem))))
                               (t (let ((key (stap-key elem)))
                                    (cons key (stap-default-value key))))))))
    ((or keyword number string) json)))

(defun stap-key (string)
  (cond ((not (stringp string))
         (error "Unexpected non-string for STAP key: ~S." string))
        ((and (not (zerop (length string))) (eql (char string 0) #\_))
         (intern (string-upcase (subseq string 1)) :keyword))
        (t string)))

(defun stap-default-value (key)
  (cond ((member key '(:i :i1 :i2))
         1)
        ((keywordp key)
         nil)
        ((not (stringp key))
         (error "Illegal STAP key ~S" key))
        (t "")))

(defun write-stap (stap &optional (stream *standard-output*))
  (st-json:write-json (pack-stap stap) stream)
  (terpri stream)
  (finish-output stream))

(defun pack-stap (stap)
  (etypecase stap
    (list (apply #'st-json:jso (iter (for (car . cdr) :in stap)
                                     (nconcing (list (de-stap-key car) (pack-stap cdr))))))
    ((or keyword number string) stap)))

(defun de-stap-key (thing)
  (if (and (keywordp thing)
           (not (typep thing 'st-json:json-bool))
           (not (typep thing 'st-json:json-null)))
      (format nil "_~(~A~)" thing)
      thing))

(defparameter *default-port* 6666)

(defparameter *default-timeout* 30)

(defclass task ()
  ((version :accessor task-version :initform nil)
   (goal :accessor task-goal :initform nil)
   (display :accessor task-display :initform nil)
   (host :accessor task-host :initarg :host)
   (port :accessor task-port :initarg :port)
   (timeout :accessor task-timeout :initarg :timeout)
   (socket :accessor task-socket :initform nil)
   (feedback :accessor task-feedback :initform nil)
   (good-feedback :accessor task-good-feedback :initform nil))
  (:default-initargs :host "localhost" :port *default-port* :timeout *default-timeout*))

(defun run-task (&rest keys &key
                              (class 'task)
                              ((:verbose *verbose*) *verbose*)
                              &allow-other-keys)
  (unless (subtypep class 'task)
    (error "~S is not a subtype of task." class))
  (let ((result (apply #'make-instance class :allow-other-keys t keys)))
    (apply #'run result keys)
    result))

(defgeneric send (task key value))

(defmethod send ((task task) key value)
  (verbose "sending ~S : ~S" key value)
  (if-let ((socket (task-socket task)))
    (write-stap `((,key . ,value)) (usocket:socket-stream socket))
    (error "No socket for task (~S ~S)." key value)))

(defgeneric run (task &rest keys &key &allow-other-keys))

(defmethod run ((task task) &rest keys &key)
  (declare (ignore keys))
  (unwind-protect
       (iter (initially (setf (task-socket task)
                              (usocket:socket-connect (task-host task)
                                                      (task-port task)
                                                      :timeout (task-timeout task)))
                        (verbose "Connected to ~A on port ~A"
                                 (task-host task) (task-port task)))
             (for (values stap success) := (read-stap (usocket:socket-stream
                                                       (task-socket task))))
             (while success)
             (verbose "  => ~S" stap)
             (update task stap))
    (when (task-socket task)
      (usocket:socket-close (task-socket task))
      (setf (task-socket task) nil))))

(defmethod (setf task-goal) :after (value (task task))
  ;; TODO This boldly assumes the _task is a "good"/"bad" one, the other possible cases
  ;;      still need to be implemented, changing the rather kludgey way this currently
  ;;      works.
  (match value
    ((assoc "good" (list (cons var val)) :test equal)
     (setf (task-feedback task) var)
     (setf (task-good-feedback task) val))))

(defgeneric display-wait (task duration))

(defmethod display-wait (task duration)
  ;; the default does nothing
  (declare (ignore task duration)))

(defgeneric deliver-feedback (task success))

(defmethod deliver-feedback (task success)
  ;; the default does nothing
  (declare (ignore task success)))

(defgeneric update (task stap))

(defgeneric update-display (task value))

(defmacro check-existing (description form new-value)
  (once-only (new-value)
    `(when-let ((#0=#:previous-value (shiftf ,form ,new-value)))
       (warn "~A from STAP server reset from ~S to ~S." ,description #0# ,new-value))))

(defmethod update ((task task) stap)
  (match stap
    (:null
     (verbose "clearing display")
     (setf (task-display task) nil))
    ((assoc :task g)
     (verbose "setting task goal to ~S" g)
     (check-existing "Task goal" (task-goal task) g))
    ((assoc :w (list (cons key value)))
     (verbose "waiting ~S seconds" value)
     (display-wait task value)
     (send task key 0))
    ((assoc :ver v)
     (verbose "expecting version ~S" v)
     (check-existing "Version" (task-version task) v))
    ((assoc :error e)
     (warn "Error sent by STAP server: ~A" e))
    ((or (assoc :template _) (assoc :unloadwarn _))
     (verbose "ignoring ~S" stap))
    ((list (cons (equal (task-feedback task)) value))
     (verbose "processing feedback ~S" value)
     (deliver-feedback task (equal value (task-good-feedback task))))
    (otherwise
     (verbose "updating display ~S" stap)
     (update-display task stap))))

(defmethod update-display ((task task) value)
  (nconcf (task-display task) (list value)))
