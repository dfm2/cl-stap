cl-stap
=======

Cl-stap is a small libary of Common Lisp code for enabling cognitive
models written in the ACT-R cognitive architecture to interact with
other software on the same or another machine using STAP over TCP.

Documentation of Voorhees, including information on downloading and
installing it, is available at

http://bitbucket.org/dfm2/cl-stap




