;;; Copyright (c) 2016 Carnegie Mellon University
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;;; software and associated documentation files (the "Software"), to deal in the Software
;;; without restriction, including without limitation the rights to use, copy, modify,
;;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;;; permit persons to whom the Software is furnished to do so, subject to the following
;;; conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in all copies
;;; or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
;;; OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(defpackage :cl-stap/test
  (:shadowing-import-from :alexandria :set-equal)
  (:use :common-lisp :cl-stap :alexandria :iterate :lisp-unit2)
  (:export :test-cl-stap)
  (:import-from :cl-stap :read-stap))

(in-package :cl-stap/test)

(defun test-cl-stap ()
  (with-summary ()
    (run-tests :package :cl-stap/test)))

(define-test test-read-stap ()
  (labels ((test-once-fn (stap lisp &optional emptyp)
             (with-input-from-string (s stap)
               (let ((read (multiple-value-list (read-stap s))))
                 (assert-eql 2 (length read))
                 (cond (emptyp (assert-equal '(nil nil) read))
                       (t (assert-true (second read))
                          (assert-equal lisp (first read))))))))
    (macrolet ((test-once (stap lisp &optional emptyp)
                 `(test-once-fn ',stap ',lisp ',emptyp)))
      (test-once "{}" nil)
      (test-once "null" :null)
      (test-once "true" :true)
      (test-once "false" :false)
      (test-once "{\"foo\":\"bar\"}" (("foo" . "bar")))
      (test-once "[[\"foo\",\"bar\"]]" (("foo" . "bar")))
      (test-once "[\"foo\",\"bar\"]" (("foo" . "") ("bar" . "")))
      (test-once "[\"foo\", \"_foo\", \"_i\", \"_i1\", \"_i2\", \"_i3\"]"
                 (("foo" . "") (:foo . nil)  (:i . 1) (:i1 . 1)
                  (:i2 . 1) (:i3 . nil)))
      (test-once "{\"foo\":[[\"\",17],[\"bar\",{\"\":22}]]}"
                 (("foo" . (("" . 17) ("bar" . (("" . 22)))))))
    )))
