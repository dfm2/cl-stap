SOURCES=cl-stap.lisp tests.lisp extract-documentation.lisp

all: TAGS documentation

documentation: doc/cl-stap.pdf doc/cl-stap.info doc/cl-stap.html doc/cl-stap/index.html

pdf: doc/cl-stap.pdf

html: doc/cl-stap.html doc/cl-stap/index.html

info: doc/cl-stap.info

doc/cl-stap.pdf: doc/cl-stap.texi doc/inc/cl-stap-version.texi
	cd doc; makeinfo --pdf cl-stap.texi

doc/cl-stap.info: doc/cl-stap.texi doc/inc/cl-stap-version.texi
	cd doc; makeinfo cl-stap.texi

doc/cl-stap.html: doc/cl-stap.texi doc/inc/cl-stap-version.texi
	cd doc; makeinfo --html --css-include=cl-stap.css --no-split cl-stap.texi

doc/cl-stap/index.html: doc/cl-stap.texi doc/inc/cl-stap-version.texi
	cd doc; makeinfo --html --css-include=cl-stap.css --split=chapter cl-stap.texi

doc/inc/cl-stap-version.texi: $(SOURCES) cl-stap.asd extract-documentation.lisp
	ccl -Q \
	-e '(ql:quickload :cl-stap/doc)' \
	-e '(cl-stap/doc:extract-documentation :cl-stap)' \
	-e '(quit)'

tidy:
	-rm -r doc/inc doc/cl-stap.aux doc/cl-stap.fn doc/cl-stap.fns doc/cl-stap.log doc/cl-stap.toc \
	       doc/cl-stap.vr doc/cl-stap.vrs doc/cl-stap.tp doc/cl-stap.tps doc/cl-stap.cp doc/cl-stap.cps

clean: tidy
	-rm -r doc/cl-stap doc/cl-stap.pdf doc/cl-stap.info doc/cl-stap.html

TAGS: $(SOURCES)
	etags $(SOURCES)

touch:
	touch doc/cl-stap.texi
